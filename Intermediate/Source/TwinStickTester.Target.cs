using UnrealBuildTool;

public class TwinStickTesterTarget : TargetRules
{
	public TwinStickTesterTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("TwinStickTester");
	}
}
